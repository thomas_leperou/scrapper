// dependencies
const fs = require('fs');
const createCsvStringifier = require('csv-writer').createObjectCsvStringifier;

/* globals */
let writeStream, csvStringifier;
const csvPath = './attendees.csv';


/**
 * @description Close the stream when the process is done
 * @type {callback}
 * @param {Array} attendeeIds
 */
const endFn = () => {
  writeStream.end();
  console.log('[OK] Parse and write to CSV done.');
}


/**
 * @description Display error and close the stream
 * @type {callback}
 * @param {Error} err
 */
const errorFn = err => {
  console.error('An error occured');
  console.error(err);
  writeStream.end();
};


/**
 * @description Write on the stream new attendees
 * @type {callback}
 * @param {Error} err
 */
const dataFn = attendees => {
  writeStream.write(csvStringifier.stringifyRecords(attendees));
  console.log(`${attendees.length} attendees have been parsed`);
};


/**
 * @description Parse the remote data and open write stream
 * @type {callback}
 * @param {Error} err
 */
const startFn = () => {
  console.log('Parse and write to CSV start:');
  csvStringifier = createCsvStringifier({
    header: [
      {id: 'id', title: 'ID'},
      {id: 'name', title: 'Name'}
    ]
  });
  writeStream = fs.createWriteStream(csvPath);
}


// Instanciate the parser
const Parser = require('./parser');
const parsePages = new Parser();
// Listen to the basic events
parsePages.on('start', startFn);
parsePages.on('end', endFn);
parsePages.on('error', errorFn);
parsePages.on('data', dataFn);
// Start the parse process
parsePages.start();
