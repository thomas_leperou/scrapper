const fs = require('fs');
const EventEmitter = require('events');

/**
 * @description
 * @type {class}
 * @extends {EventEmitter}
 */
module.exports = class Parser extends EventEmitter {

  constructor() {
    super();

    this.page = 0;
    this.attendeeIds = [];

  }


  start() {
    this.getPage(this.page);
    this.emit('start');
  }


  /**
   * @description recursive function to retrieve the ids
   *              of attendees for each page
   * @return {void}
   */
  getPage() {
    fs.stat(`./-dummy/data${this.page}.json`, (err, res) => {
      if(!err) {
        const attendees = require(`./-dummy/data${this.page}.json`).loaderData.peopleData.map(i => i.id_object);
        this.getPage(this.page++);
        this.getAttendees(attendees);
      } else {
        this.emit('end');
      }
    });
  }


  /**
   * @description Retrieve async data of an attendee
   * @return {Promise} contain the attendee data
   */
  getAttendee(id) {
    return new Promise(resolve => resolve({ id, name: `Attendee ${id}` }));
  }


  /**
   * @description Execute all promises related to attendees data
   *              and emit the event end
   * @return {void}
   */
  getAttendees(attendees) {
    Promise
      .all(attendees.map(this.getAttendee))
      .then(attendees => this.emit('data', attendees))
      .catch(err => this.emit('error', err));
  }


}
